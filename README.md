# Module2 - Master Basic skills

### Goal of the module 
1. Understand basics of DNN and CNN
2. Basic working of the AI techniques for :
   - Face Detection 
   - Face Recognition
   - Face Detection with 106landmarks
   - Object Detection
   - Image classification

### How to complete this Module
You will learn  about these technologies from the internet and submit a summary the 20 hour way to anchor the concept for yourself . 
More details are in the summary issue : 
You need to submit atleast 3 Techniques though you may submit all. We will check all and best ones will be featured in the community with credits to you .

1. Check Study material in [wiki](https://gitlab.com/iotiotdotin/project-internship-ai/module2-master-basic-skills/-/wikis/home)
2. You need to submit a summary assignments which you will get in [issues](https://gitlab.com/iotiotdotin/project-internship-ai/module2-master-basic-skills/-/issues/).

